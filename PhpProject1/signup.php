<?php
    include_once 'header.php';
?>
        <section class="main-container">
            <div class="main-wrapper">
                <h2>Signup</h2>
                
                <form class="signup-form" action="includes/signup.inc.php" method="POST">
                    <?php 
                        if (isset($_GET['first'])) {
                            $first = $_GET['first'];
                            echo '<input type="text" name="first" placeholder="Firstname" value="'.$first.'">';
                        }
                        else{
                            echo '<input type="text" name="first" placeholder="Firstname">';
                        }
                        
                        if (isset($_GET['last'])) {
                            $last = $_GET['last'];
                            echo '<input type="text" name="last" placeholder="Lastname" value="'.$last.'">';
                        }
                        else{
                            echo '<input type="text" name="last" placeholder="Lastname">';
                        }
                        
                    ?>
    
                    <input type="text" name="email" placeholder="e-mail">
                    
                    <?php
                    if (isset($_GET['uid'])) {
                            $uid = $_GET['uid'];
                            echo '<input type="text" name="uid" placeholder="Username" value="'.$uid.'">';
                        }
                        else{
                            echo '<input type="text" name="uid" placeholder="Username">';
                        }
                    ?>

                    <input type="password" name="pwd" placeholder="Password">
                    
                    
                    
                
                <?php
                
                if(!isset($_GET['signup'])){
                    
                }
                else{
                    $signupCheck = $_GET['signup'];
                    
                    if($signupCheck == "empty"){
                        echo "<p class='error'>You did not fill in all fields!</p>";
                        
                    }
                    elseif($signupCheck == "invalid"){
                        echo "<p class='error'>You used invalid characters!</p>";
                        
                    }
                    elseif($signupCheck == "email"){
                        echo "<p class='error'>You used an invalid email!</p>";
                        
                    }
                    elseif($signupCheck == "success"){
                        echo "<p class='success'>You have been signed up!</p>";
                        
                    }
                }
                
                ?>
                    <br>
                    <h3>Are you a WORKER or a REQUESTER?</h3>
                    <br> </br>
                    <h3>    Worker:</h3>
                    <input type="radio" name="lvl" value="User" checked="checked">
                    <br><br>
                    <h3>    Requester:</h3>
                    <input type="radio" name="lvl" value="Admin">
                    
                    <button type="submit" name="submit">Sign up</button>
                </form>      
                
            </div> 
        </section>
        <?php
            include_once 'footer.php';
        ?>

