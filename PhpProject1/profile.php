<html>
    <head>
        <title><?php echo $uid; ?>'s profile</title>
    </head>
    <body>

<?php

include_once 'includes/dbh.inc.php';
include_once 'header.php';

if(isset($_POST['username'])){
    
        
        $username = $_POST['username'];

        $sql = "SELECT * FROM users WHERE user_uid='$username'";

        $result = mysqli_query($conn, $sql);
        $resultCheck = mysqli_num_rows($result);

        if ($resultCheck != 1) {
            die("That username could not be found");
        }

        while($row = mysqli_fetch_assoc($result)){
            $first = $row['user_first'];
            $last = $row['user_last'];
            $email = $row['user_email'];
            $uid = $row['user_uid'];
            $lvl = $row['user_lvl'];
            
            if($lvl=='User'){

    ?>  
            <h2><?php echo "$uid"; ?>'s profile</h2>
            <br>

            <table>
                <tr><td>Firstname: <?php echo $first;?></td></tr>
                <tr><td>Lastname: <?php echo $last;?></td></tr>
                <tr><td>Username: <?php echo $uid;?></td></tr>
                <tr><td>Email: <?php echo $email;?></td></tr>
            </table>

    <?php       

        }else{
            die("This username doesn't refer to a worker");
        }
    }//while
    
    }else die("You must specify a username");
?> 
        
</body>
</html>

