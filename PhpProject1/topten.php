<?php
    include_once 'header.php';
    include_once 'includes/dbh.inc.php';    
?>
        <section class="main-container">
            <div class="main-wrapper">
                        
                
                
                <!-- _______________________________________ADMIN__________________________________________-->
                 <?php
                
                    if(isset($_SESSION['u_id'])){
                        if($_SESSION['u_lvl']=='Admin'){
                            
                 ?>
                
                <h2>TOP 10 WORKERS</h2>    
                
                <?php
                            
                            if(isset($_GET['enter'])){
                            $enter_id = $_GET['enter'];
                                        
                            }else{
                                $enter_id = $row['post_id'];
                            }
                            
                            
                            $sql_topTenTask = "SELECT users.user_uid , SUM(t_u.score) AS totalScore , SUM(t_u.success) AS totalSuccess , SUM(t_u.failed) AS totalFailed
                                               FROM t_u, users
                                               WHERE t_u.user_id = users.user_id AND t_u.post_id = $enter_id
                                               GROUP BY user_uid
                                               ORDER BY SUM(t_u.score) DESC
                                               LIMIT 10";
                            
                            $result = mysqli_query($conn, $sql_topTenTask);
                            $resultCheck = mysqli_num_rows($result);
                            
                            
                            $leaderboard = array();
                            
                            if($resultCheck > 0){
                                
                                $count = 1;
                                while($row = mysqli_fetch_assoc($result)) {
                                    $leaderboard[$count] = $row;
                                    $count++;
                                    
                                }
                            }
                            
                            for($i=1;$i<$count;$i++){
                                        echo "#$i <br>";
                                        echo 'Username: '; print_r($leaderboard[$i]['user_uid']);
                                        echo '<br>';
                                        echo 'Score in this campaign: '; print_r($leaderboard[$i]['totalScore']);
                                        echo '<br>';
                                        echo 'Tasks succeed: '; print_r($leaderboard[$i]['totalSuccess']);
                                        echo '<br>';
                                        echo 'Tasks failed: '; print_r($leaderboard[$i]['totalFailed']);
                                        echo '<br><br><br>';
                                        
                                    }
                            
                        
                        }//if($_SESSION['u_lvl']=='Admin')

                        
                    }//if(isset($_SESSION['u_id'])
                ?>
                
                
    
          
                <?php
                        if(isset($_SESSION['u_id'])){
                            if($_SESSION['u_lvl']=='User'){
                                
                ?>
                
                <h2>Your progress</h2>    
                
                <?php
                                
                                $username = $_SESSION['u_uid'];

                                if(isset($_GET['enter'])){
                                    $enter_id = $_GET['enter'];

                                }else{
                                    $enter_id = $row['post_id'];
                                }


                                $sql_topTenTask = "SELECT users.user_uid , SUM(t_u.score) AS totalScore , SUM(t_u.success) AS totalSuccess , SUM(t_u.failed) AS totalFailed
                                                   FROM t_u, users
                                                   WHERE t_u.user_id = users.user_id AND t_u.post_id = $enter_id
                                                   GROUP BY user_uid
                                                   ORDER BY SUM(t_u.score) DESC";
                                                   

                                $result = mysqli_query($conn, $sql_topTenTask);
                                $resultCheck = mysqli_num_rows($result);


                                $leaderboard = array();

                                if($resultCheck > 0){

                                    $count = 1;
                                    while($row = mysqli_fetch_assoc($result)) {
                                        $leaderboard[$count] = $row;
                                        $count++;

                                    }
                                }

                                for($i=1;$i<$count;$i++){
                                    if($leaderboard[$i]['user_uid'] == $username){
                                        echo "Your leaderboard rank is: #$i <br>";
                                        echo 'Username: '; print_r($leaderboard[$i]['user_uid']);
                                        echo '<br>';
                                        echo 'Your score in this campaign: '; print_r($leaderboard[$i]['totalScore']);
                                        echo '<br>';
                                        echo 'Your tasks succeed: '; print_r($leaderboard[$i]['totalSuccess']);
                                        echo '<br>';
                                        echo 'Your tasks failed: '; print_r($leaderboard[$i]['totalFailed']);
                                        echo '<br><br><br>';
                                    }

                                        }


                            }//if($_SESSION['u_lvl']=='User')

                        
                    }//if(isset($_SESSION['u_id'])
                                 
                                ?>
        
        
            </div>
            
        </section>

        <?php
            include_once 'footer.php';
        ?>
