<?php
    include_once 'header.php';
    include_once 'includes/dbh.inc.php';  
?>
        <section class="main-container">
            <div class="main-wrapper">
                <h2>YOUR TASKS</h2>
                
                 <?php
                
                    if(isset($_SESSION['u_id'])){
                        if($_SESSION['u_lvl']=='Admin'){
                        //Requester
                           
                            
                             ?>
                <html>
                    
                    <form action="includes/task.inc.php" method="POST">
                    
                    <h3>Create a task</h3>
                    
                    <?php
                    
                            if(isset($_GET['enter'])){
                            $enter_id = $_GET['enter'];
                                        
                            }else{
                                $enter_id = $row['post_id'];
                            }
                            
                    ?>
                    
                    <br> <br>
                    Send the task and then add the possible answers
                    <br> <br>
                    <form action="includes/post.inc.php" method="POST">
                      
                    Title:<br>
                    <textarea rows="2" cols="15" name="title" placeholder="Write title task here"></textarea>
                    <br><br>
                    Description: <br>
                    <textarea rows="2" cols="30" name="description" placeholder="Write description task here"></textarea>
                    
                    <br><br>
                    
                    Minimum workers
                    <input type="number" name="workers" min="2" max="100">
                    <br><br>
                    
                    Min. rate answer validation
                    <input type="number" name="valid" min="1" max="100">%
                    <br><br>
                    
                    <input type="hidden" name="post_id" value="<?php echo $enter_id; ?>" />
                    
                    Send the task:
                        <button type="submit" name="send_task">Send</button>
                    
                    </form>
                     
                    <br><br>_______________________________________________________
                    <br><br><br>
                    
                    <?php
                    
                    $sql_showInfo = "SELECT * FROM taskinfo WHERE post_id = '$enter_id'";
                                        
                                        $resultInfo = mysqli_query($conn, $sql_showInfo);
                                        $resultCheckInfo = mysqli_num_rows($resultInfo);
                                    
                                        if($resultCheckInfo > 0){
                                            while($rowInfo = mysqli_fetch_assoc($resultInfo)) {
                                                
                    
                    ?>
                    
                    
                    
                                                <h4>Total tasks: <?php echo $rowInfo['task_progress'] + $rowInfo['task_completed'] + $rowInfo['task_failed']; ?> </h4>

                                                <h4>Tasks under execution: <?php echo $rowInfo['task_progress']; ?> </h4>

                                                <h4>Tasks completed: <?php echo $rowInfo['task_completed']; ?> </h4>

                                                <h4>Tasks failed: <?php echo $rowInfo['task_failed']; ?> </h4>
                                                
                                                <h4>Campaign completion: <?php if($rowInfo['task_progress']>0){echo 100-($rowInfo['task_progress']/($rowInfo['task_progress'] + $rowInfo['task_completed'] + $rowInfo['task_failed'])*100);}else{echo '100';} ?>% </h4>
                    
                    
                                            <?php
                                            }//while
                                        }//if
                                            
                                            ?>
                    
                    
                    <br><br>
                    _______________________________________________________<br>
                    Remember you should change interests and skills only if you can't complete tasks
                    <br><br><br>
       
                    <?php 
                                
                                        $sql_show = "SELECT * FROM task WHERE post_id = '$enter_id'";
                                        
                                        $result = mysqli_query($conn, $sql_show);
                                        $resultCheck = mysqli_num_rows($result);
                                        
                          
                                        if($resultCheck > 0){
                                            while($row = mysqli_fetch_assoc($result)) {
                                                
                                                ?>
                                
                                                <br><br>
                                                
                                                <?php $task_id = $row['task_id']; ?>
                                                <h3>Task id: <?php echo $task_id; ?></h3>
                                                
                                                
                                                <input type="hidden" name="post_id" value="<?php echo $enter_id; ?>" />
                                                <input type="hidden" name="task" value="<?php echo $task_id; ?>" />
                                                
                                                <?php $currentWorkers = $row['task_currentWorkers'];?>
                                                <?php $workers = $row['task_workers']; ?>
                                                <?php $valid = $row['task_valid']; ?>
                                                
                                                <h3>Title: <?php echo $row['task_title']; ?></h3>
                                                <h3>Description: <?php echo $row['task_description']; ?></h3>
                                                
                                                <h4>Workers: <?php echo $row['task_currentWorkers']; ?>/<?php echo $row['task_workers']; ?></h4>
                                                <h4>Min rate validation: <?php echo $row['task_valid']; ?>%</h4>
                                                <h4>Interest: <?php echo $row['task_interest']; ?></h4>
                                                <h4>Skill: <?php echo $row['task_skill']; ?></h4>
                                                
                                                <?php  
                                                
                                                    $sql_interests = "SELECT * FROM interests";
                                                    $result_interests = mysqli_query($conn, $sql_interests);

                                                    while($row_interests = mysqli_fetch_assoc($result_interests)){

                                                    ?>
                                                
                                                <form action="includes/interest.inc.php" method="POST">
                                                    <table>
                                                        
                                                        <select name="interests">
                                                                <?php
                                                                    
                                                                    while($row_interests = mysqli_fetch_assoc($result_interests)){
                                                                ?> 
                                                                
                                                                <option value="<?php echo $row_interests['interest_value']?>"><?php echo $row_interests['interest_value']?></option>
                                                                
                                                                <?php
                                                                    }
                                                                ?> 
                                                                <input type="hidden" name="post_id" value="<?php echo $enter_id; ?>" />
                                                                <input type="hidden" name="task" value="<?php echo $task_id; ?>" />
                                                            </select>
                                                        <button type="submit" id="int" name="int">Set new most important interest</button><br>
                                                        
                                                    </table> 
                                                </form>
                                                
                                                
                                                <?php       

                                                    }//while
                                                ?>
                                                
                                                     
                                                <?php
                                                    
                                                    $sql_skills = "SELECT * FROM skills";
                                                    $result_skills = mysqli_query($conn, $sql_skills);

                                                    while($row_skills = mysqli_fetch_assoc($result_skills)){

                                                    ?> 

                                                            <form action="includes/skill.inc.php" method="POST">
                                                            <table>
                                                                <select name="skills">
                                                                        <?php
                                                                            while($row_skills = mysqli_fetch_assoc($result_skills)){
                                                                        ?> 
                                                                        
                                                                        <option value="<?php echo $row_skills['skill_value']?>"><?php echo $row_skills['skill_value']?></option>
                                                                        
                                                                        <?php
                                                                            }
                                                                        ?> 
                                                                        <input type="hidden" name="post_id" value="<?php echo $enter_id; ?>" />
                                                                        <input type="hidden" name="task" value="<?php echo $task_id; ?>" />
                                                                    </select>
                                                                <button type="submit" id="ski" name="ski">Set new most important skill</button><br>
                                                                
                                                                
                                                            </table> 
                                                            </form>     
                                                    <?php       

                                                    }//while


                                                    ?>
                                                       
                                                <form action="includes/answer.inc.php" method="POST">
                                                    
                                                     
                                                    <input type="hidden" name="post_id" value="<?php echo $enter_id; ?>" />
                                                    <input type="hidden" name="task" value="<?php echo $task_id; ?>" />
                                                    
                                                    <h6>Answers: <?php// echo $row['task_answer']; ?></h6>
                                                    <input type="text" name="answer" placeholder="add an answer">
                                                    <button type="submit" name="answer_content">Add</button> 
                                                    
                                                    </form>
                                                
                                                <?php 
                                 
                                      
                                                    $sql_answer = "SELECT * FROM answers WHERE task_id = '$task_id'";

                                                    $answer_result = mysqli_query($conn, $sql_answer);
                                                    $answer_resultCheck = mysqli_num_rows($answer_result);

                                                    
                                                    if($answer_resultCheck > 0){
                                                        while($row = mysqli_fetch_assoc($answer_result)) {
                                                            $answer_content =  $row['answer_content'];
                                                                 
                                                    ?>       
                                                    
                                                    <input type="radio" name="answer" value=<?php $answer_content?> checked><?php echo $answer_content?><br>
                                                     
                                                    <!-- show answers-->
                                                
                                                    <?php            
                                                        }//while($row = mysqli_fetch_assoc($result))
                                                    }//if($resultCheck > 0)
                                                
                                                ?>
                                                  
                                    <?php 
                                    
                                    if($currentWorkers == $workers){
                                           
                                        //get most frequent answer
                                        $sql_correctAnswer = "SELECT answer_content , updated , COUNT(answer_content) AS correctAnswer
                                        FROM t_u
                                        WHERE task_id = '$task_id'
                                        GROUP BY answer_content
                                        ORDER BY correctAnswer DESC
                                        LIMIT 1";

                                        $correctAnswer_result = mysqli_query($conn, $sql_correctAnswer);
                                        $correctAnswer_resultCheck = mysqli_num_rows($correctAnswer_result);
                                        
                                        if($correctAnswer_resultCheck > 0){
                                            while($rowCorrectAnswer = mysqli_fetch_assoc($correctAnswer_result)) {
                                                $totalCorrectAnswers = $rowCorrectAnswer['correctAnswer'];
                                                $correctAnswer = $rowCorrectAnswer['answer_content'];
                                                $updated = $rowCorrectAnswer['updated'];
                                            }
                                        }
                                        
                                        $answerRate = ($totalCorrectAnswers/$workers)*100;
                                        
                                        if($answerRate > $valid){
                                            
                                            
                                            $points = $answerRate/10;
                                            $pointsUpdated = false;
                                            
                                            ?>
                                                    
                                                    <form action="includes/score.inc.php" method="POST">
                                                        <input type="hidden" name="task" value="<?php echo $task_id; ?>" />
                                                        <input type="hidden" name="correctAnswer" value="<?php echo $correctAnswer; ?>" />
                                                        <input type="hidden" name="points" value="<?php echo $points; ?>" />
                                                        <input type="hidden" name="post_id" value="<?php echo $enter_id; ?>" />
                                                        <input type="hidden" name="pointsUpdated" value="<?php echo $pointsUpdated; ?>" />
                                                        
                                                        
                                                        <?php if($updated < 1) { ?>
                                                        <button type="submit" name="score">Update workers score</button>
                                                        <?php }else{ ?> 
                                                        <button type="submit" name="score" disabled>Score updated</button>
                                                        <?php } ?>
                                                        
                                                        
                                                    </form>
                                                    
                                                    
                                            <?php
                                            
                                            
                                            //info update
                                            if($updated==1){
                                                $sql_updateInfo = "UPDATE taskinfo SET task_completed = task_completed + 1 , task_progress = task_progress - 1 WHERE post_id = '$enter_id';"; 
                                                mysqli_query($conn, $sql_updateInfo);
                                                $sql_stopUpdate = "UPDATE t_u SET updated = 3 WHERE task_id = '$task_id';"; 
                                                mysqli_query($conn, $sql_stopUpdate);  
                                            }
                                            
                                            echo $pointsUpdated;
                                            echo'SUCCESS |';
                                            echo ' Most common answer was: '.$correctAnswer.' |';
                                            echo ' With validation rate = '.$answerRate.'%';
                                            
                                            
                                        }else{
                                            
                                            ?>
                                                    
                                                 <form action="includes/failed.inc.php" method="POST">
                                                        <input type="hidden" name="task" value="<?php echo $task_id; ?>" />
                                                        <input type="hidden" name="correctAnswer" value="<?php echo $correctAnswer; ?>" />
                                                        <input type="hidden" name="points" value="<?php echo $points; ?>" />
                                                        <input type="hidden" name="post_id" value="<?php echo $enter_id; ?>" />
                                                        <input type="hidden" name="pointsUpdated" value="<?php echo $pointsUpdated; ?>" />
                                                        
                                                        
                                                        <?php if($updated < 1) { ?>
                                                        <button type="submit" name="score">Update workers score</button>
                                                        <?php }else{ ?> 
                                                        <button type="submit" name="score" disabled>Update workers score</button>
                                                        <?php } ?>   
                                                </form>
                                                    
                                                    
                                            <?php
                                            
                                           
                                            
                                            //info update
                                            if($updated==2){
                                                $sql_updateInfo = "UPDATE taskinfo SET task_failed = task_failed + 1 , task_progress = task_progress - 1 WHERE post_id = '$enter_id';"; 
                                                mysqli_query($conn, $sql_updateInfo);
                                                $sql_stopUpdate = "UPDATE t_u SET updated = 3 , failed=1 WHERE task_id = '$task_id';"; 
                                                mysqli_query($conn, $sql_stopUpdate);
                                            }
                                            
                                            echo'task failed';
                                            
                                        }
                                                             
                                    } 
                                    
                                            }//while($row = mysqli_fetch_assoc($result))
                                        }//if($resultCheck > 0)
                            
                                    ?>
                    
                    <?php
                    
                        }//if($_SESSION['u_lvl']=='Admin')
                        
                    ?>
                                                    
                    <?php
                    
                        if($_SESSION['u_lvl']=='User'){
                            
                            $user_id = $_SESSION['u_id'];
                            
                            if(isset($_GET['enter'])){
                            $enter_id = $_GET['enter'];
                                        
                            }else{
                                $enter_id = $row['post_id'];
                            }
                            
                        
                        
                        $sql_show = "SELECT task.*
                                    FROM task , users
                                    WHERE 
                                              task.post_id = '$enter_id' AND
                                              users.user_id = '$user_id' AND    
                                              (users.user_interest = task.task_interest OR 
                                               users.user_skill = task.task_skill)
                                    ORDER BY task_id DESC";
                        
                        
                                        
                                        $result = mysqli_query($conn, $sql_show);
                                        
                                        $resultCheck = mysqli_num_rows($result);
                                        
                          
                                        if($resultCheck > 0){
                                            while($row = mysqli_fetch_assoc($result)) {
                                                ?>
                                
                                                <br><br>
                                                
                                                <?php $task_id = $row['task_id']; ?>
                                                <?php $currentWorkers = $row['task_currentWorkers'];?>
                                                <?php $workers = $row['task_workers']; ?>
                                                
                                                <h3>Task id: <?php echo $task_id; ?></h3>
                                                
                                                <input type="hidden" name="post_id" value="<?php echo $enter_id; ?>" />
                                                <input type="hidden" name="task" value="<?php echo $task_id; ?>" />
                                                
                                                
                                                <h3>Title: <?php echo $row['task_title']; ?></h3>
                                                <h3>Description: <?php echo $row['task_description']; ?></h3>
                                                <h4>Workers: <?php echo $row['task_currentWorkers']; ?>/<?php echo $row['task_workers']; ?></h4>
                                                <h4>Min rate validation: <?php echo $row['task_valid']; ?>%</h4>
                                                <h4>Interest: <?php echo $row['task_interest']; ?></h4>
                                                <h4>Skill: <?php echo $row['task_skill']; ?></h4>
                                                <!-- your answer -->
                                                
                                                
                                                <?php
                                                $sql_answer = "SELECT * FROM answers WHERE task_id = '$task_id'";

                                                    $answer_result = mysqli_query($conn, $sql_answer);
                                                    $answer_resultCheck = mysqli_num_rows($answer_result);

                                                    
                                                    if($answer_resultCheck > 0){
                                                        while($row = mysqli_fetch_assoc($answer_result)) {
                                                            $answer_content =  $row['answer_content'];
                                        
                                                                 
                                                    ?>       
                                                    <form action="includes/getAnswer.inc.php" method="post">
                                                    <input type="radio" name="answer_content" value="<?php echo $answer_content?>" checked><?php echo $answer_content?><br>
                                                    
                                                    
                                                    <?php
                                                    }//while for results
                                                        }//if for results
                                                         
                                                    ?>
                                                    
                                                        <input type="hidden" name="currentWorkers" value="<?php echo $currentWorkers; ?>" />
                                                        <input type="hidden" name="workers" value="<?php echo $workers; ?>" />
                                                        <input type="hidden" name="task" value="<?php echo $task_id; ?>" />
                                                        <input type="hidden" name="post_id" value="<?php echo $enter_id; ?>" />
                                                        
                                                        
                                                        
                                                        <?php if($currentWorkers < $workers) { ?>
                                                        <button type="submit" name="send_answer" >Send Answer</button>
                                                        <?php }else{ ?> 
                                                        <button type="submit" name="send_answer" disabled>Done</button>
                                                        <?php } ?>
                                                        
                                                    </form>
                            
                                        <?php
                                                        
                                                        
                                               if(isset($_POST['send_answer'])){
                                                   $resultCheck--;
                                               }               
                                                      
                    
                                            }//while
                                        }else{
                                            echo 'No task is suitable with your interest or skill';
                                        }
                                        
                        }//if==User
                    
                    ?>
                        
                        
                <?php        
                    }//if(isset($_SESSION['u_id'])
                ?>
                  
                                <br>
                            </body>
                        </html>
         
            </div>
            
        </section>

        <?php
            include_once 'footer.php';
        ?>
