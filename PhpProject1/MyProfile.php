<html>
    <head>
        <title>Your profile</title>
    </head>
    <body>

<?php

        include_once 'includes/dbh.inc.php';
        include_once 'header.php';

        $username = $_SESSION['u_uid'];
        $id = $_SESSION['u_id'];
        

        $sql = "SELECT * FROM users WHERE user_uid='$username'";
        $result = mysqli_query($conn, $sql);
        $resultCheck = mysqli_num_rows($result);
        
        
        $sql_interests = "SELECT * FROM interests";
        $result_interests = mysqli_query($conn, $sql_interests);
        
        
        $sql_skills = "SELECT * FROM skills";
        $result_skills = mysqli_query($conn, $sql_skills);
        
        if($_SESSION['u_val']==1){

        while($row = mysqli_fetch_assoc($result)){
            $first = $row['user_first'];
            $last = $row['user_last'];
            $email = $row['user_email'];
            $uid = $row['user_uid'];
            $lvl = $row['user_lvl'];
            $interest = $row['user_interest'];
            $skill = $row['user_skill'];
            
    ?>  
            <h2>Your profile</h2>
            <br>
            
            <form action="MyProfile.php" method="POST">
            
            <table>
                Firstname: <?php echo $first;?>
                <button type="submit" name="m_first">Modify firstname</button><br>
                Lastname: <?php echo $last;?>
                <button type="submit" name="m_last">Modify lastname</button><br>
                Username: <?php echo $uid;?>
                <button type="submit" name="m_user">Modify username</button><br>
                Email: <?php echo $email;?>
                <button type="submit" name="m_email">Modify email</button><br>
                Favorite interest: <?php echo $interest;?><br>
                Favorite skill: <?php echo $skill;?><br><br><br>
            </table>

                
            </form>    
    <?php       

    }//while
         
    while($row_interests = mysqli_fetch_assoc($result_interests)){
            
    ?>  
             <h2>Change your interest:</h2>
            <br>
            <form action="MyProfile.php" method="POST">
            <table>
                Interests:
                <select name="interests">
                        <?php
                            while($row_interests = mysqli_fetch_assoc($result_interests)){
                        ?> 
                    
                        <option value="<?php echo $row_interests['interest_value']?>"><?php echo $row_interests['interest_value']?></option>
                        
                        <?php
                            }
                        ?> 
                        
                    </select>
                <button type="submit" name="int">Select interest</button><br><br><br>
            </table> 
            </form>    
    <?php       

    }//while
            
    while($row_skills = mysqli_fetch_assoc($result_skills)){
        
    ?> 
            
             <h2>Change your skill:</h2>
            <br>
            <form action="MyProfile.php" method="POST">
            <table>
                Skills:
                <select name="skills">
                        <?php
                            while($row_skills = mysqli_fetch_assoc($result_skills)){
                        ?> 
                    
                        <option value="<?php echo $row_skills['skill_value']?>"><?php echo $row_skills['skill_value']?></option>
                        
                        <?php
                            }
                        ?> 
                        
                    </select>
                <button type="submit" name="ski">Select skill</button><br>
            </table> 
            </form>     
    <?php       

    }//while
    
    
    ?>
    
    
    <!--___________CHANGE FIRSTNAME___________________________-->
    
    <?php
    
    if(isset($_POST['m_first'])){
        
        ?>
        <br><br><br><br>    
        
        <form action="MyProfile.php" method="POST">
        Change here your firstname:    
        <input type="text" name="first" placeholder="Firstname">
        <button type="submit" name="changeFirst">Change</button>
        </form>
        <br><br> 
        
    <?php    
    
    }
        
        
        if(isset($_POST['changeFirst'])){
            
                
                $id = $_SESSION['u_id'];
                $first = $_POST['first'];
             
                if(preg_match("/^[a-zA-Z]*$/",$first)){
                    
                $sql_changeFirst = mysqli_query($conn , "UPDATE users SET user_first='$first' WHERE user_id='$id';");
                echo 'Done!';
                }else{
                    echo 'Name not valid';
                }
        }
    
    ?> 
        
        
    <!--___________CHANGE LASTNAME___________________________-->
    
    <?php
    
    if(isset($_POST['m_last'])){
        
        ?>
        <br><br><br><br>    
        
        <form action="MyProfile.php" method="POST">
        Change here your lastname:    
        <input type="text" name="last" placeholder="Lastname">
        <button type="submit" name="changeLast">Change</button>
        </form>
        <br><br> 
        
    <?php    
    
    }
        
        
        if(isset($_POST['changeLast'])){
           
            $id = $_SESSION['u_id'];
            $last = $_POST['last'];
            
            if(preg_match("/^[a-zA-Z]*$/",$last)){
            
            $sql_changeLast = mysqli_query($conn , "UPDATE users SET user_last='$last' WHERE user_id='$id';");
            echo 'Done!';
            
            }else{
                echo 'name not valid';
            }
        }
    
    ?> 
<!--___________CHANGE USERNAME___________________________-->    
    <?php
    
    if(isset($_POST['m_user'])){
        
        ?>
        <br><br><br><br>    
        
        <form action="MyProfile.php" method="POST">
        Change here your username:    
        <input type="text" name="username" placeholder="Username">
        <button type="submit" name="changeUsername">Change</button>
        </form>
        <br><br> 
        
    <?php    
    
    }
        
        
        if(isset($_POST['changeUsername'])){
            $id = $_SESSION['u_id'];
            $new_username = $_POST['username'];
            
            $sql = "SELECT * FROM users WHERE user_uid='$uid'";  
            $result = mysqli_query($conn, $sql);
            $resultCheck = mysqli_num_rows($result);
            
            if($resultCheck > 0){
               echo 'This username is already taken , please try again';   
            }else{
            
            $sql_changeUsername = mysqli_query($conn , "UPDATE users SET user_uid='$new_username' WHERE user_id='$id';");
            echo 'Logout and then login with your new username to see the changes';
            }
        }
            
    ?> 
        
<!--___________CHANGE EMAIL___________________________-->   


    
    <?php
    
    if(isset($_POST['m_email'])){
        
        ?>
        <br><br><br><br>    
        
        <form action="MyProfile.php" method="POST">
        Change here your email:    
        <input type="text" name="email" placeholder="email">
        <button type="submit" name="changeEmail">Change</button>
        </form>
        <br><br> 
        
    <?php    
    
    }
        
        
        if(isset($_POST['changeEmail'])){
            
            $id = $_SESSION['u_id'];
            $email = $_POST['email'];
            
            if(filter_var($email, FILTER_VALIDATE_EMAIL)){  
                
            $sql = "SELECT * FROM users WHERE user_email='$email'";  
            $result = mysqli_query($conn, $sql);
            $resultCheck = mysqli_num_rows($result);
            
                if($resultCheck > 0){
                   echo 'This email is already taken , please try again';   
                }else{

                $sql_changeEmail = mysqli_query($conn , "UPDATE users SET user_email='$email' WHERE user_id='$id';");
                echo 'Done!';
                }
            

            }else{
                echo 'Please insert a valid email';
            }
        }
        
        ?>
        
        <!--___________CHANGE INTEREST___________________________-->
    
    <?php
    
        if(isset($_POST['int'])){
            
                
                $id = $_SESSION['u_id'];
                $interest = $_POST['interests'];
             
                $sql_changeInterest = mysqli_query($conn , "UPDATE users SET user_interest='$interest' WHERE user_id='$id';");
                
                
                echo 'Remember to refresh if you changed both';
                
        }
    
    ?> 
        
        <!--___________CHANGE SKILL___________________________-->
    
    <?php
    
        if(isset($_POST['ski'])){
            
                
                $id = $_SESSION['u_id'];
                $skill = $_POST['skills'];
             
                $sql_changeSkill = mysqli_query($conn , "UPDATE users SET user_skill='$skill' WHERE user_id='$id';");
                
                
                echo 'Remember to refresh if you changed both';
                
        }
    
    ?> 
        
    <?php
    
    }else{
        
        while($row = mysqli_fetch_assoc($result)){
            $first = $row['user_first'];
            $last = $row['user_last'];
            $email = $row['user_email'];
            $uid = $row['user_uid'];
            $lvl = $row['user_lvl'];
            
    ?>  
            <h2>Your profile</h2>
            <br>
            
            <form action="MyProfile.php" method="POST">
            
            <table>
                Firstname: <?php echo $first;?><br>
                Lastname: <?php echo $last;?><br>
                Username: <?php echo $uid;?><br>
                Email: <?php echo $email;?><br><br>
            </table>
                
                

               
            </form>    
    <?php       

        }//while
        echo'You will be able to modify your profile when the admin will accept your request';
    
    }//else    
    
    ?>
         
</body>
</html>
 