-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Set 17, 2018 alle 01:23
-- Versione del server: 10.1.35-MariaDB
-- Versione PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zagre`
--

DELIMITER $$
--
-- Procedure
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `getTasks` (IN `id` INT(11))  NO SQL
SELECT task.*
FROM task , users
WHERE 
	  task.post_id = id AND
	  (users.user_interest = task.task_interest OR 
	   users.user_skill = task.task_skill)
ORDER BY task_id DESC$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Struttura della tabella `answers`
--

CREATE TABLE `answers` (
  `answer_id` int(11) NOT NULL,
  `answer_content` varchar(60) NOT NULL,
  `task_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `answers`
--

INSERT INTO `answers` (`answer_id`, `answer_content`, `task_id`) VALUES
(19, 'vaporwave', 11),
(20, 'anime', 11),
(21, 'memes', 11),
(22, 'jeans', 12),
(23, 'cotton', 12),
(24, 'wool', 12),
(25, 'hitman', 13),
(26, 'norton', 13),
(27, 'kaspersky', 13),
(28, 'Nvidia', 14),
(29, 'AMD', 14);

-- --------------------------------------------------------

--
-- Struttura della tabella `interests`
--

CREATE TABLE `interests` (
  `interest_id` int(11) NOT NULL,
  `interest_value` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `interests`
--

INSERT INTO `interests` (`interest_id`, `interest_value`) VALUES
(1, 'Music'),
(2, 'Paint'),
(3, 'Sport'),
(4, 'Technology');

-- --------------------------------------------------------

--
-- Struttura della tabella `post`
--

CREATE TABLE `post` (
  `post_id` int(11) NOT NULL,
  `post_content` varchar(600) DEFAULT NULL,
  `post_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `post_deadline` date DEFAULT NULL,
  `post_creator` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `post`
--

INSERT INTO `post` (`post_id`, `post_content`, `post_date`, `post_deadline`, `post_creator`) VALUES
(19, 'Campaign about design on clothes', '2018-09-16 22:15:56', '2018-03-09', 'beppe1518'),
(22, 'Information for the new company I am working for', '2018-09-16 22:38:32', '2019-02-11', 'jerry420');

-- --------------------------------------------------------

--
-- Struttura della tabella `skills`
--

CREATE TABLE `skills` (
  `skill_id` int(11) NOT NULL,
  `skill_value` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `skills`
--

INSERT INTO `skills` (`skill_id`, `skill_value`) VALUES
(1, 'Classification'),
(2, 'Design'),
(3, 'Critically'),
(4, 'Coding');

-- --------------------------------------------------------

--
-- Struttura della tabella `task`
--

CREATE TABLE `task` (
  `task_id` int(11) NOT NULL,
  `task_title` varchar(600) NOT NULL,
  `task_description` varchar(600) NOT NULL,
  `task_interest` varchar(20) NOT NULL,
  `task_skill` varchar(20) NOT NULL,
  `task_workers` int(11) NOT NULL,
  `task_currentWorkers` int(11) NOT NULL,
  `task_valid` int(11) NOT NULL,
  `post_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `task`
--

INSERT INTO `task` (`task_id`, `task_title`, `task_description`, `task_interest`, `task_skill`, `task_workers`, `task_currentWorkers`, `task_valid`, `post_id`) VALUES
(11, 'Illustration Style', 'Which of these styles should I choose for a black t-shirt', 'Paint', 'Design', 8, 7, 60, 19),
(12, 'Pants', 'Which of these fabrics should I choose for long pants', 'Sport', 'Critically', 3, 3, 80, 19),
(13, 'Antivirus', 'Which antivirus should I install on the computers of the company?', 'Technology', 'Coding', 5, 5, 65, 22),
(14, 'Video card', 'which type of video card should I install on the computers of the company?\r\nThe company creates videogames', 'Technology', 'Critically', 6, 6, 80, 22);

-- --------------------------------------------------------

--
-- Struttura della tabella `taskinfo`
--

CREATE TABLE `taskinfo` (
  `post_id` int(11) NOT NULL,
  `task_progress` int(11) NOT NULL,
  `task_completed` int(11) NOT NULL,
  `task_failed` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `taskinfo`
--

INSERT INTO `taskinfo` (`post_id`, `task_progress`, `task_completed`, `task_failed`) VALUES
(19, 1, 1, 0),
(22, 2, 0, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `t_u`
--

CREATE TABLE `t_u` (
  `connection` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `answer_content` varchar(20) NOT NULL,
  `score` int(11) NOT NULL,
  `success` int(11) NOT NULL,
  `failed` int(11) NOT NULL,
  `updated` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `t_u`
--

INSERT INTO `t_u` (`connection`, `post_id`, `task_id`, `user_id`, `answer_content`, `score`, `success`, `failed`, `updated`) VALUES
(40, 22, 14, 19, 'Nvidia', 0, 0, 0, 0),
(41, 22, 13, 19, 'hitman', 0, 0, 0, 0),
(42, 19, 11, 19, 'vaporwave', 0, 0, 0, 0),
(43, 19, 12, 20, 'cotton', 10, 1, 0, 3),
(44, 19, 11, 20, 'anime', 0, 0, 0, 0),
(45, 19, 12, 21, 'cotton', 10, 1, 0, 3),
(46, 22, 13, 21, 'hitman', 0, 0, 0, 0),
(47, 19, 12, 22, 'cotton', 10, 1, 0, 3),
(48, 19, 11, 22, 'memes', 0, 0, 0, 0),
(49, 22, 14, 22, 'Nvidia', 0, 0, 0, 0),
(50, 22, 14, 23, 'Nvidia', 0, 0, 0, 0),
(51, 22, 13, 23, 'hitman', 0, 0, 0, 0),
(52, 19, 11, 24, 'vaporwave', 0, 0, 0, 0),
(53, 19, 11, 25, 'anime', 0, 0, 0, 0),
(54, 22, 14, 26, 'AMD', 0, 0, 0, 0),
(55, 22, 13, 26, 'hitman', 0, 0, 0, 0),
(56, 22, 14, 27, 'Nvidia', 0, 0, 0, 0),
(57, 19, 11, 27, 'anime', 0, 0, 0, 0),
(58, 19, 11, 28, 'vaporwave', 0, 0, 0, 0),
(59, 22, 14, 29, 'Nvidia', 0, 0, 0, 0),
(60, 22, 13, 29, 'norton', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_first` varchar(256) NOT NULL,
  `user_last` varchar(256) NOT NULL,
  `user_email` varchar(256) NOT NULL,
  `user_uid` varchar(256) NOT NULL,
  `user_pwd` varchar(256) NOT NULL,
  `user_lvl` enum('Admin','User','Super') NOT NULL DEFAULT 'User',
  `user_validation` int(11) NOT NULL,
  `user_interest` varchar(10) NOT NULL,
  `user_skill` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `users`
--

INSERT INTO `users` (`user_id`, `user_first`, `user_last`, `user_email`, `user_uid`, `user_pwd`, `user_lvl`, `user_validation`, `user_interest`, `user_skill`) VALUES
(19, 'One', 'One', 'one1@gmail.com', 'one', '$2y$10$r6TAeqyOaob5Uy6Qsa.cnuuViqn9z.mvOk6DBF/uFH8//ghmTdB2m', 'User', 1, 'Technology', 'Design'),
(20, 'Two', 'Two', 'two2@gmail.com', 'two', '$2y$10$bM8g/A/XD6zcWdmkUXSeEe3M0zX/p5u/Hdu5yi89FIkFcxwdC9rz2', 'User', 1, 'Sport', 'Design'),
(21, 'Three', 'Three', 'three3@gmail.com', 'three', '$2y$10$u.oLUYrZ6rlT2gqeEEViquCU6Xy7CfX6vy2bDt1lyqhKx/LTgGv4u', 'User', 1, 'Sport', 'Coding'),
(22, 'Four', 'Four', 'four4@gmail.com', 'four', '$2y$10$WcAj4/g78Pijf7j1dGxgBOpTtwG8hT8NIxJEybO2M.rVPXnEU549m', 'User', 1, 'Paint', 'Critically'),
(23, 'Five', 'Five', 'five5@gmail.com', 'five', '$2y$10$s5LkELcLGNreIdYG4O1So.Dp0oTtetzn8ywLb4DbQhoIiWCPoq4I6', 'User', 1, 'Technology', 'Critically'),
(24, 'Six', 'Six', 'six6@gmail.com', 'six', '$2y$10$.CxK3qnnNq.hOnS2gTLxBOHoSJaLXiEzqecLEr7hCFFucgA49CpMS', 'User', 1, 'Paint', 'Design'),
(25, 'Seven', 'Seven', 'seven7@gmail.com', 'seven', '$2y$10$UsCiQJTFHzMExHeov1Kenei3fOIck0TSxk7dujNuJGthsTgaD3A3.', 'User', 1, 'Sport', 'Design'),
(26, 'Eight', 'Eight', 'eight8@gmail.com', 'eight', '$2y$10$gyw/.zMop5lpyFxokz3J7uXrcsHc8Y.c.uNEHjqt0LwqX3NNAt2Zu', 'User', 1, 'Technology', 'Critically'),
(27, 'Nine', 'Nine', 'nine9@gmail.com', 'nine', '$2y$10$9hLaZ6/21qoPhlE7bgxoqeka7lkxlTenlNrd9hFhM3orWpvoEmjFe', 'User', 1, 'Paint', 'Critically'),
(28, 'Ten', 'Ten', 'ten10@gmail.com', 'ten', '$2y$10$LIamfE/LSPlXhHjHoP401u3mbnIfRSIP2Dml0sCC8Oz0f/jNlmrmi', 'User', 1, 'Paint', 'Design'),
(29, 'Eleven', 'Eleven', 'eleven11@gmail.com', 'eleven', '$2y$10$e2x5nEfQ1KLu7SzPHTs2nuTrZnQUXT3EeZvuePMdXo.sOa4l0PzSa', 'User', 1, 'Technology', 'Critically'),
(30, 'Twelve', 'Twelve', 'twelve12@gmail.com', 'twelve', '$2y$10$DClQ1ib9kG6HwhYQ/NcLueKRGfEdjX5huyVPGMFeqpwom5IlQ.9kC', 'User', 1, 'Sport', 'Coding'),
(31, 'Jerry', 'Scotti', 'jerry420@gmail.com', 'jerry420', '$2y$10$.xfA9EFJlWBbUDDLZmsK/ebEwGShr.Z7.qr/NhFi9TG7DZu1zcg.6', 'Admin', 1, '', ''),
(32, 'Beppe', 'Fischioni', 'beppe1518@gmail.com', 'beppe1518', '$2y$10$itZJAuYYiUVJrJXI61YZXutpWrcY7D6/0/7hDariY3kM335GVurg2', 'Admin', 1, '', ''),
(33, 'Admin', 'Admin', 'admin@gmail.com', 'admin', '$2y$10$7cOWI4px0OL1qGrN55A6.e2xlVh8TM8K3q1kUkZj0K8o48TSjI5HC', 'Super', 1, '', '');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`answer_id`);

--
-- Indici per le tabelle `interests`
--
ALTER TABLE `interests`
  ADD PRIMARY KEY (`interest_id`);

--
-- Indici per le tabelle `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`post_id`);

--
-- Indici per le tabelle `skills`
--
ALTER TABLE `skills`
  ADD PRIMARY KEY (`skill_id`);

--
-- Indici per le tabelle `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`task_id`);

--
-- Indici per le tabelle `taskinfo`
--
ALTER TABLE `taskinfo`
  ADD PRIMARY KEY (`post_id`);

--
-- Indici per le tabelle `t_u`
--
ALTER TABLE `t_u`
  ADD PRIMARY KEY (`connection`);

--
-- Indici per le tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `answers`
--
ALTER TABLE `answers`
  MODIFY `answer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT per la tabella `interests`
--
ALTER TABLE `interests`
  MODIFY `interest_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT per la tabella `post`
--
ALTER TABLE `post`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT per la tabella `skills`
--
ALTER TABLE `skills`
  MODIFY `skill_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT per la tabella `task`
--
ALTER TABLE `task`
  MODIFY `task_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT per la tabella `t_u`
--
ALTER TABLE `t_u`
  MODIFY `connection` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT per la tabella `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
